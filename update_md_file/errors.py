class BaseError(Exception):
    """Base error class to inherit."""


class LineInvalidTypeError(BaseError):
    """Invalid line index."""
