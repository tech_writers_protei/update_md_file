from os.path import relpath
from pathlib import Path
from typing import Iterable, Iterator

from loguru import logger

from dir_file import DirFile, MdFile


class FileDict:
    """
    The dictionary of the files inside the directory.

    Attributes
    ----------
    _root_dir : str | Path
        The directory path.
    _dict_files : dict[Path, DirFile | MdFile]
        The dictionary of the paths and the files.

    """
    def __init__(self, root_dir: str | Path):
        self._root_dir: Path = Path(root_dir).resolve()
        self._dict_files: dict[Path, DirFile | MdFile] = dict()

    def __contains__(self, item):
        if not isinstance(item, (str, Path)):
            return False
        return Path(item) in self._dict_files

    def __add__(self, other):
        def _add_file(__path: str | Path):
            file_path: Path = Path(__path).resolve()
            if file_path in self:
                logger.info(f"Path {file_path} is already listed")
                return

            dir_file: DirFile = DirFile(self._root_dir, file_path)
            if bool(dir_file):
                md_file: MdFile = MdFile(dir_file)
                self[file_path] = md_file
            else:
                self[file_path] = dir_file
            return

        if isinstance(other, (str, Path)):
            _add_file(other)
        elif isinstance(other, Iterable):
            for item in filter(lambda x: isinstance(x, (str, Path)), other):
                _add_file(item)
        else:
            logger.info(f"Элемент {other} должен быть типа str или Path, но получено {type(other)}")
        return

    __radd__ = __add__
    __iadd__ = __add__

    def __iter__(self) -> Iterator[MdFile]:
        return iter(filter(lambda x: isinstance(x, MdFile), self._dict_files.values()))

    def __getitem__(self, item):
        if not isinstance(item, (str, Path)):
            logger.exception(
                f"Элемент {item} должен быть типа str или Path, но получено {type(item)}", result=True)
            return
        if item not in self:
            _relpath: str = relpath(item, self._root_dir.parent.parent).replace("\\", "/")
            logger.error(f"Файл по пути {_relpath} не найден", result=True)
            return
        return self._dict_files.get(Path(item))

    def __setitem__(self, key, value):
        if isinstance(key, (str, Path)) and isinstance(value, (DirFile, MdFile)):
            self._dict_files[key] = value
        else:
            logger.info(
                f"Ключ {key} должен быть типа str или Path, а значение {value} типа DirFile или MdFile, "
                f"но получены {type(key)} и {type(value)}")
