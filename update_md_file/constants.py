from pathlib import Path
from re import Pattern
from textwrap import dedent
from typing import TypeAlias

StrPath: TypeAlias = str | Path
_EXTENSION: str = ".md"
_LOG_FOLDER: Path = Path.home().joinpath("Desktop").joinpath("_logs")
_VERSION: str = "1.0.0"

_PATTERN_IMAGE: Pattern = compile(r"^!\[\"([^]]*)\"]\((.*)\)")
_IMAGE: str = dedent("""\
\n<style>
image-with-caption {
  display: block;
  text-align:center;
  white-space: nowrap;
}
image-with-caption > img {
  border: 1px solid gray;
}
image-with-caption > p {
  margin-top: 1px;
}
</style>\n
""")


def generate_image(path: str, caption: str):
    anchor: str = Path(path).stem.lower().replace("_", "-")
    return dedent(f"""\
<image-with-caption>
  <img src="{path}">
  <a name="{anchor}"><p>{caption}</p></a>
</image-with-caption>
""")
