from re import Match, match
from typing import Iterator

from constants import _IMAGE, _PATTERN_IMAGE, generate_image
from update_md_file.dir_file import MdFile


class ImageUpdate:
    def __init__(self, md_file: MdFile):
        self._md_file: MdFile = md_file
        self._indexes: list[int] = []

    def __repr__(self):
        return repr(self._md_file)

    __str__ = __repr__

    @property
    def _content(self):
        return self._md_file.content

    def read(self):
        self._md_file.read()

    def __bool__(self):
        return any(line.startswith("![") for line in iter(self))

    def __iter__(self) -> Iterator[str]:
        return iter(self._content)

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._content[item]
        else:
            raise KeyError

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._content[key] = value
        else:
            raise TypeError

    def add_css(self):
        indexes: list[int] = [index for index, line in enumerate(iter(self)) if line.startswith("---")]
        self._content.insert(indexes[1] + 1, _IMAGE)

    def fix_image(self):
        for index, line in enumerate(iter(self)):
            _m: Match = match(_PATTERN_IMAGE, line)
            if _m:
                path: str = _m.group(1)
                caption: str = _m.group(2)
                self[index] = generate_image(path, caption)
        return

    def repair(self):
        self.read()
        if bool(self):
            self.add_css()
            self.fix_image()
            self._md_file.content = self._content
            self._md_file.write()
